From: Debian Forensics <forensics-devel@lists.alioth.debian.org>
Date: Tue, 14 Mar 2017 10:38:27 +0100
Subject: fix-version.py

---
 rekall-gui/setup.py | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/rekall-gui/setup.py b/rekall-gui/setup.py
index a26e530..e64063c 100755
--- a/rekall-gui/setup.py
+++ b/rekall-gui/setup.py
@@ -27,7 +27,7 @@ import os
 from setuptools import find_packages, setup, Command
 
 VERSION_ENV = {}
-exec(open("rekall_gui/_version.py").read(), VERSION_ENV)
+exec open("_version.py").read() in VERSION_ENV
 VERSION = VERSION_ENV["get_versions"]()
 
 rekall_description = "Rekall Memory Forensic Framework"
